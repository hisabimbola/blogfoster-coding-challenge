# Coding Challenge


## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:
* Git - [Download & Install Git](https://git-scm.com/downloads). OSX and Linux machines typically have this already installed.
* Node.js - [Download & Install Node.js (LTS)](https://nodejs.org/en/download/) and the npm package manager. If you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.

## Setup

### Cloning
- Clone this project
```
git clone git@bitbucket.org:hisabimbola/blogfoster-coding-challenge.git
```

### Project structure

```
.
├── app
│   ├── README.md
│   ├── node_modules
│   ├── package-lock.json
│   ├── package.json
│   ├── public
│   └── src
└── server
    ├── README.md
    ├── app.js
    ├── config.js
    ├── controllers
    ├── node_modules
    ├── package-lock.json
    ├── package.json
    ├── routes
    ├── server.js
    ├── test-config.json
    └── tests
```

`app` contains all the files related to the web app, while `server` contains all the files related to the server.

### Web App
_NB: The webapp uses the [create-react-app](https://github.com/facebookincubator/create-react-app) boilerplate_

#### Install
Navigate to the `/app` and install dependencies with this command
```
npm install
```

#### Running the application
To start the web and watch, run

```
npm start
```

#### Testing
Test are written with Jest. To run tests run:

```
npm test
```

### Server

#### Install
Navigate to the `/server` and install dependencies with this command
```
npm install
```

#### Running the application
To start the web, run

```
npm start
```

To restart the server during development, run

```
npm run start:watch
```

#### Testing
Test are written with Jest. To run tests run:

```
npm test
```

To watch files changed and run test, run:

```
npm run test:watch
```
