import fetch from 'isomorphic-fetch'

export const CREATE_SHORT_LINK = 'CREATE_SHORT_LINK'
export const RECEIVE_LINK = 'RECEIVE_LINK'
export const BASE_URL = 'http://localhost:8888/v1'
export function createShortLink(body) {
  return dispatch => {
    console.log(body, 'in createShortLink');
    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
    return fetch(`${BASE_URL}/links`, options)
      .then(res => res.json())
      .then(json => dispatch(receiveLink(json)))
  }
}

export function receiveLink(link) {
  return {
    type: RECEIVE_LINK,
    link: link.hash,
  }
}
