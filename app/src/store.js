import { createStore, applyMiddleware } from 'redux'
import { fromJS } from 'immutable';
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './reducers'


const configureStore = (initialState) => createStore(
  rootReducer,
  fromJS(initialState),
  composeWithDevTools(
    applyMiddleware(
      thunkMiddleware,
    ),
  )
);

export default configureStore;
