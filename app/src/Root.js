import React, { Component } from 'react'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import configureStore from './store'
import HomePage from './containers/Homepage'
import Header from './components/Header'
import './App.css';

export default class Root extends Component {
  render() {
    return (
      <Provider store={configureStore({})}>
        <Router>
          <div>
            <Header />
            <Route path='/' component={HomePage}></Route>
          </div>
        </Router>
      </Provider>
    )
  }
}
