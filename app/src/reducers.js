import { combineReducers } from 'redux-immutable'
import homepageReducer from './containers/Homepage/reducer'

const rootReducer = combineReducers({
  homepage: homepageReducer,
})

export default rootReducer
