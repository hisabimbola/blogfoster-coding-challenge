export const API_BASE_URL = 'http://localhost:8888/v1'
export const BASE_URL = 'http://localhost:8888'

export const CREATE_SHORT_LINK = 'app/Homepage/CREATE_SHORT_LINK'
export const RECEIVE_LINK = 'app/Homepage/RECEIVE_LINK'
