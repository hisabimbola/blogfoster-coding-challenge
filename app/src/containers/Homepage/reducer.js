import { fromJS } from 'immutable'
import {
  BASE_URL,
  RECEIVE_LINK,
} from './constants'

const initialState = fromJS({
  link: '',
});

export default function homepageReducer(state = initialState, action) {
  switch(action.type) {
    case RECEIVE_LINK:
      return state.set('link', `${BASE_URL}/${action.link}`)
    default:
      return state
  }
}
