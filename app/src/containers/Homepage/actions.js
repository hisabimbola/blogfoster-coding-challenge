import fetch from 'isomorphic-fetch'
import {
  API_BASE_URL,
  RECEIVE_LINK,
} from './constants'



export function createShortLink(body) {
  return dispatch => {
    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
    return fetch(`${API_BASE_URL}/links`, options)
      .then(res => res.json())
      .then(json => dispatch(receiveLink(json)))
  }
}

export function receiveLink(link) {
  return {
    type: RECEIVE_LINK,
    link: link.hash,
  }
}
