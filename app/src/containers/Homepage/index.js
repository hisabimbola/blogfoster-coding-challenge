import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createShortLink } from './actions';
import selectHomepage from './selectors';

export class Homepage extends Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const link = this.refs.linkInput.value
    this.props.dispatch(createShortLink({ url:link }))
  }

  render() {
    return (
      <div>
        <form ref="linkForm" onSubmit={this.handleSubmit}>
          <input type="text" ref="linkInput" placeholder='Paste a link to shorten it' />
          <button type='submit'>Shorten</button>
        </form>

        {!!this.props.link && <p>{this.props.link}</p>}
      </div>
    );
  }
}

Homepage.propTypes = {
  createShortLink: PropTypes.func.isRequired,
  link: PropTypes.string,
}

const mapStateToProps = () => selectHomepage();

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  createShortLink: (body) => dispatch(createShortLink(body)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);
