import { createSelector } from 'reselect';


const selectHomepageDomain = (state) => state.get('homepage');

const selectHomepage = (state) => createSelector(
  selectHomepageDomain,
  (substate) => substate.toJS(),
);

export {
  selectHomepageDomain,
}


export default selectHomepage;
