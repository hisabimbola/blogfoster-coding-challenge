module.exports = {
    "env": {
        "es6": true,
        "node": true,
        "jest/globals": true
    },
    "plugins": [
      "jest"
    ],
    "extends": "standard",
    "rules": {
        "comma-dangle": ["error", "always-multiline"],
        "jest/no-disabled-tests": "error",
        "jest/no-focused-tests": "error",
        "jest/no-identical-title": "error",
        "jest/valid-expect": "error"
    }
};
