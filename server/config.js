const config = {
  development: {
    env: process.env.NODE_ENV || 'development',
    server: {
      port: process.env.SERVER_PORT || 8888,
    },
    web: {
      port: process.env.WEB_PORT || 8889,
    },
  },
  test: {
    env: process.env.NODE_ENV || 'test',
    server: {
      port: process.env.SERVER_PORT || 5555,
    },
    web: {
      port: process.env.WEB_PORT || 5554,
    },
  },
}

function initConfig (env) {
  return config[env]
}

const env = process.env.NODE_ENV || 'development'
module.exports = initConfig(env)
