const express = require('express')
const router = express.Router()
const linkController = require('../../controllers')

router
  .post('/links', linkController.createShortLink)
  .delete('/links/:hash', linkController.deleteHash)

module.exports = router
