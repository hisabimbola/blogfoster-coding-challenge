const express = require('express')
const router = express.Router()
const linkController = require('../controllers')
/* GET home page. */
router.get('/', (req, res) => res.json({hello: 'world'}))
router.get('/:hash', linkController.redirectToOriginal)

module.exports = router
