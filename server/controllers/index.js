'use strict'

const shortid = require('shortid')
const debug = require('debug')('link:ctrl')

const store = (function initStore () {
  return new Map()
})()

exports.createShortLink = function (req, res) {
  const { url } = req.body

  if (!url) {
    debug('Error: Missing url in request body')
    return res.status(400).json({
      errMessage: 'Missing required url parameter',
    })
  }

  const id = getValidId(store)
  debug('Mapped link: %s to %s', url, id)
  store.set(id, url)
  debug('Saved id in db.')
  return res.json({
    hash: id,
  })
}

exports.redirectToOriginal = function (req, res) {
  const shortUrlHash = req.params.hash
  const origUrl = store.get(shortUrlHash)
  if (!origUrl) {
    return res.status(404).json({ errMessage: 'Hash not found' })
  }
  res.redirect(origUrl)
}

function getValidId (store) {
  const id = shortid.generate() // we can as well, write a powerful hashing algorithm.
  if (store.has(id)) {
    return getValidId(store)
  }
  return id
}

exports.deleteHash = function (req, res) {
  const shortUrlHash = req.params.hash
  if (!store.has(shortUrlHash)) {
    return res.status(400).json({ errMessage: 'Hash not found' })
  }
  store.delete(shortUrlHash)
  return res.status(200).end()
}
