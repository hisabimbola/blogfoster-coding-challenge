const request = require('supertest')
const app = require('../app')

describe('Routes', () => {
  describe('Home', () => {
    it('should response the GET method', () => {
      return request(app).get('/').then((response) => {
        expect(response.statusCode).toBe(200)
        expect(response.body).toEqual({ hello: 'world' })
      })
    })
  })
  describe('LinkShortener', () => {
    it('should shorten the url and return.', () => {
      return request(app)
        .post('/v1/links')
        .send({ url: 'http://google.com' })
        .set('accept', 'json')
        .then((response) => {
          expect(response.statusCode).toBe(200)
          expect(response.body).toEqual({
            hash: expect.any(String),
          })
        })
    })
    describe('Redirect', () => {
      let testHash
      const testUrl = 'www.example.com'
      beforeEach(() => {
        return request(app)
          .post('/v1/links')
          .send({ url: testUrl })
          .set('accept', 'json')
          .then(({ body: { hash } }) => {
            testHash = hash
          })
      })
      it('should redirect a valid short hash', () => {
        return request(app)
          .get(`/${testHash}`)
          .expect('Location', testUrl)
      })
    })
    describe('Delete', () => {
      let testHash
      const testUrl = 'www.example.com'
      beforeEach(() => {
        return request(app)
          .post('/v1/links')
          .send({ url: testUrl })
          .set('accept', 'json')
          .then(({ body: { hash } }) => {
            testHash = hash
          })
      })
      it('should delete a hash', () => {
        return request(app)
          .delete(`/v1/links/${testHash}`)
          .then(({ statusCode }) => {
            expect(statusCode).toBe(200)
            return request(app)
              .get(`/${testHash}`)
              .then(({ statusCode }) => {
                expect(statusCode).toBe(404) // hash is not found
              })
          })
      })
    })
  })
})
